﻿

[cmdletbinding()]
param([Switch]$ListOnly, [bool]$applyMemTimings=$true, [string]$algo)

$mypath=$PSScriptRoot

$overdrive_keys=[ordered]@{
    'SUBSYS_0B361002&REV_C1'='AMD_64'
    'SUBSYS_36811462&REV_C3'='MSI_56'
    'SUBSYS_E37F1DA2&REV_C3'='Nitro_56'
    'SUBSYS_E37F1DA2&REV_C1'='Nitro_64'
    'SUBSYS_23081458&REV_C1'='Gigabyte_64'
    }

#get cards
$cards=Get-WmiObject Win32_PNPEntity -Filter "PNPClass='Display' AND Name='Radeon RX Vega'"

$matcher=[regex]"\d+,\d+,\d+"
$byBusID=@{}
foreach ($card in $cards)
{
    $locationInfo = (get-itemproperty -path "HKLM:\SYSTEM\CurrentControlSet\Enum\$($card.PNPDeviceID)" `
        -name locationinformation).locationINformation    
    if($matcher.IsMatch($locationInfo))
    {
        $busId,$deviceID,$functionID = $matcher.Match($locationInfo).value -split "," 
        $byBusID[[int]$busId]=$card
        #locationinfo eg. @System32\drivers\pci.sys,#65536;PCI bus %1, device %2, function %3;(21,0,0)
        Write-Verbose "LocationInfo: $locationInfo"
    } else 
    {
        Write-Warning -Message "Couldnt extract bus info from $locationInfo"
    }
    
}

#TODO: Order info by the various formats, especially map BusID to OpenCL order (used by cast-xmr)
#Make sure its sorted
$byBusID=$byBusID.GetEnumerator()|sort Name

$subsysRevID=[regex]'(SUBSYS_[^\&]+&REV_\w{2,2})'
#apply overdrive
$idx=1

$overdrive_args_reset=@()
$overdrive_args=@()
$overdrive_args_reapply=@()

$iniContent=&$mypath\Get-IniContent((Join-Path $PSScriptRoot '.\OverdriveNTool\OverdriveNTool.ini'))
$byProfile=$iniContent.keys|?{$_ -match 'Profile_*'}|%{New-Object pscustomobject -Property $iniContent[$_]}|Group-Object -Property Name -AsHashTable


$list=foreach($entry in $byBusID)
{
    $devID=$entry.value.deviceid
    $key=$subsysRevID.Match($devID).Groups[1].value
    $profile=$overdrive_keys[$key]
    $overdrive_args_reset += "-r$idx"
    $overdrive_args += "-p$idx`"$profile`""
    #Also set values manually once more, bug in overdriventool
    #e.g. "-ac1" "GPU_P7=1392;963" "-ac2" "GPU_P7=1320;995" "-ac3" "GPU_P7=1392;963" 
    #now get the raw values
    $p7State = $byProfile[$profile].GPU_P7 -split ';'
    $overdrive_args_reapply += "-ac$idx"
    $overdrive_args_reapply += "GPU_P7=" + (([int]$p7State[0]) + 1) + ';' + (([int]$p7State[1]) + 1)
    New-Object psobject -Property @{
        PCIDeviceID=$devID
        BusIndex=$entry.Key
        Key=$key
        ProfileIndex=$idx
        Profile=$profile
        ProfileP7State=$p7State
    }
    $idx+=1
}

if ($ListOnly)
{
    $list|ft
    
} else {

    #TODO: Get ini values and apply seperately 
    sleep 1
    Write-Verbose "$mypath\OverdriveNTool\OverdriveNTool.exe -consoleonly @overdrive_args"
    &$mypath\OverdriveNTool\OverdriveNTool.exe -consoleonly @overdrive_args
    sleep 1
    #Write-Verbose "$mypath\OverdriveNTool\OverdriveNTool.exe -consoleonly @overdrive_args_reapply"
    #&$mypath\OverdriveNTool\OverdriveNTool.exe -consoleonly @overdrive_args_reapply
    sleep 1
    &$mypath\OverdriveNTool\OverdriveNTool.exe -consoleonly @overdrive_args
    
    if($applyMemTimings)
    {
        Write-Host Applying memory timings
        $amdmemtweak="$mypath\amdmemorytweak\win\WinAMDTweak.exe"
        $amdmemtweakProfile = @{
            'MSI_56' = "--cl 18 --ras 23 --rcdrd 23 --rcdwr 11 --rc 34 --rp 13 --rrds 3 --rrdl 4 --rtp 6 --faw 12 --cwl 7 --wtrs 4 --wtrl 4 --wr 11  --rfc 164 --REF 17000"
        }
        foreach($item in $list)
        {
            $idx=$item.ProfileIndex-1
            
            if ($amdmemtweakProfile.ContainsKey($item.Profile))
            {
                Write-Information "Applying amdmemtweak profile for $($item.Profile)"
                $profile = $amdmemtweakProfile[$item.Profile]
                $memTweakArgs="--gpu $idx $profile"
                Write-host "$amdmemtweak $memTweakArgs"
                &$amdmemtweak @($memTweakArgs -split ' ')
            }
            else
            {
                Write-Warning "Couldnt find amdmemtweak profile for $($item.Profile) "
            }
        }
    }

}


