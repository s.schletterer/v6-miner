#!/bin/bash
set -eux
#cardNo=1

find VegaToolsNConfigs/ -name "*.sh" -exec chmod 775 \{} \;

#Get vega cards -thx https://github.com/dasunsrule32/radeon-scripts/blob/master/vega-fan-control
AMDSYSFILE="pp_table"
SYSPATHS=$(find /sys/devices -name $AMDSYSFILE -type f -exec dirname {} \;) # Autodetect SYSFS path.

declare -a cards
for syspath in $SYSPATHS
do
    card=$(find ${syspath}/drm -maxdepth 1 -name card* -type d -exec basename '{}' ';')
    cardNo=${card#card}
    echo Found card $cardNo: $syspath
    cards[$cardNo]=$syspath
done

cd VegaToolsNConfigs/tools
for cardNo in ${!cards[*]}
do
./setPPT.sh ${cardNo} ~/v6-miner/config/V56PPT1270M900V862PP75HX1 
./setAMDGPUFanSpeed.sh -s77 -g${cardNo}
#./setSMClockVoltages.sh  $cardNo ../config/eth/MSIV56H.data_OC8_7 

done
cd -
./teamredminer-linux/teamredminer -a ethash -o stratum+tcp://europe.ethash-hub.miningpoolhub.com:20535 -u narciss.ubuntu -p x #--cn_config 16+14
#./moniterGPU.sh ${cardNo}
