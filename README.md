## Miner for Monero

Started as a generic miner, now only monero. New Miner is **TeamRedMiner**


## Windows

 * Power Settings: High Performance - No sleep !!!
 * Suppress start up recovery! - no auto boot !!! ```bcdedit /set {current} recoveryenabled no```
 * Swap Mem: 64 Gb
 * Enable large Pages in memory
 * Always search non-indexed
 * Disable fast boot

### Threat Protection

  * Add exemption for C:\mining


### Disable auto repair

## Ubuntu

### Setup

1. sudo echo "vm.nr_hugepages = 128" > /etc/sysctl.d/60-hugepages.conf
2. sudo apt install -y openssh-server
3. sudo apt install lm-sensors
4. Set up auto logon
5. sudo apt install vim
6. echo "10" |sudo tee /proc/sys/kernel/hung_task_timeout_secs


##### AMD

```GRUB_CMDLINE_LINUX_DEFAULT="quiet splash amdgpu.ppfeaturemask=0xfffd7fff amdgpu.vm_block_size=10 amdgpu.vm_size=1024"```

Where `amdgpu.vm_` is for teamredminer mode C mining

#### Python

1. `py -m pip install pip --upgrade pip`
2. `py -m pip install cgminerhttpinterface`


#### Git

```bash
sudo apt install git
git config --global user.email s.schletterer@gmail.com
git config --global user.name "Stefan Schletterer"
```

Refer to VegaToolsNConfigs for setting up Vegas

### Miners

* ./ethminer -U -P stratum2+tcp://narciss.eth:x@europe.ethash-hub.miningpoolhub.com:20535

