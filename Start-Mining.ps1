﻿

param (
    [switch]$skipReset = $true
)

$mypath=Split-Path $MyInvocation.MyCommand.Path

$VerbosePreference = 'Continue'

.\Ping-Reset.ps1

#TODO: Attach to process and listen to it
$proc=$proc=Get-Process -Name (Split-Path $current_miner -Leaf)
if($proc)
{
    #found existing process, killing
    $proc.kill()
    $proc.waitforexit(1000)
}

#get cards
$cards=Get-WmiObject Win32_PNPEntity -Filter "PNPClass='Display' AND Name='Radeon RX Vega'"

#restart gpus
#$cards|%{$_.disable();sleep 1; $_.enable(); sleep 1}
if (-not $skipReset)
{
    $switchRadeon=join-path $mypath cast_xmr\switch-radeon-gpu.exe -Resolve
    &$switchRadeon --hbcc off --largepages on --admin fullrestart
}

& "$mypath\Overdrive-Apply.ps1" -applyMemTimings $false -verbose



$pool="europe.cryptonight-hub.miningpoolhub.com:20580"
$wallet= if($env:COMPUTERNAME -eq "DESKTOP-MINER-1"){'narciss.4'}else{'narciss.asrock'} 
$pw='x'

$gpu_arg=0..($cards.Length-1) -join ','

$miners=@{

    #TODO: Write config file for claymore miners
    cryptonote="'$mypath\CryptoNote\NsGpuCNMiner.exe' $mypath\cryptonote.config.txt"
    vertcoin="$mypath\lyclMiner\lyclMiner.exe"
    vertcoin_args=@()
    
    teamred="$mypath\teamredminer-win\teamredminer.exe"
    #teamred_args=("-a cnr -o stratum+tcp://europe.cryptonight-hub.miningpoolhub.com:20580 -u narciss.asrock -p x --api_listen=127.0.0.1:7777 --bus_reorder" -split ' ')
    teamred_args=("-a ethash -o stratum+tcp://europe.ethash-hub.miningpoolhub.com:20535 -u narciss.asrock -p x --api_listen=127.0.0.1:7777 --bus_reorder --log_file=$mypath\miner.log" -split ' ')


    claymore="$mypath\claymore-eth-win\EthDcrMiner64.exe"
    claymore_args=("-epool europe.ethash-hub.miningpoolhub.com:20535 -strap 1 -rxboost 1")
    
}


$env:GPU_FORCE_64BIT_PTR=1
$env:GPU_MAX_HEAP_SIZE=100
$env:GPU_USE_SYNC_OBJECTS=1
$env:GPU_MAX_ALLOC_PERCENT=100
$env:GPU_SINGLE_ALLOC_PERCENT=100

$current_miner=$miners.teamred
$current_miner_args=$miners.teamred_args


#$current_miner=$miners.vertcoin
#$current_miner_args=$miners.vertcoin_args

.\Ping-Reset.ps1

#TODO: For teamred or any sgminer compatible, start
#set PATH=%PATH%;%LOCALAPPDATA%\Programs\Python\Python37\Scripts\
#chi-server -p 7777

while ($true)
{
    #Invoke-Expression "& $current_miner 2>&1 > miner_$_.log"
    #TODO: Ping cast-xmr for disconnected, then restart
    if ($current_miner_args.count -eq 0)
    {
        Write-Host "Starting $current_miner "
        $proc=Start-Process -FilePath $current_miner -WorkingDirectory $(Split-Path $current_miner -Parent) -RedirectStandardOutput miner.log -RedirectStandardError miner.err -PassThru -Verbose:$true
    } else {
        Write-Host "Starting $current_miner $current_miner_args"
        $proc=Start-Process -FilePath $current_miner -ArgumentList $current_miner_args -PassThru -Verbose:$true    
    }
    sleep 15
    while ($true)
    {
        if ($current_miner -like $miners.cast_xmr)
        {
            $json=$false
            #Check for: Process responds with rest, still there
            $json=Invoke-RestMethod -Method GET http://localhost:7777
            if ($json)
            {
                .\Ping-Reset.ps1
                Write-host ("{0:G}: Status of pool: $($json.pool.status)" -f [DateTime]::Now)
                $json.devices|%{"{0}: {1:N0} khs" -f $_.device,($_.hash_rate/1000)}
                if ($json.pool.status -match 'disconnected')
                {
                    Write-host "Disconnect detected, restarting miner"
                    $proc.Kill()
                    $proc.WaitForExit(5000)
                    Write-host "Miner killed, restarting"
                    break;
                }
            } else {
                #no ping
            }
            
            sleep 10
    
        } else {
            #TODO: implement ping
            sleep 10
            .\Ping-Reset.ps1
            Write-Host "$([DateTime]::Now)  Sleeping for 10 seconds"
        }
    }
    
    #$json=Invoke-RestMethod -Method GET http://localhost:7777
    #$status.pool.status = connected|disconnected
}


#TODO: Reboot!





