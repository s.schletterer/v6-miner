#!/usr/bin/env python3
"""
Module Docstring
"""

__author__ = "Stefan Schletterer"
__version__ = "0.1.0"
__license__ = "MIT"

import argparse
import urllib.request
import tarfile
import logging
import json
import shutil
import tempfile
from urllib.parse import urlparse
import os
import re

#from mega import Mega

class Miner(object):
    def download(self):
        pass

class Teamredminer(Miner):
    def download(self):
        tgz_url=get_github_asset('todxx/teamredminer/releases/latest', 'teamredminer-v%s-linux.tgz') 
        download(tgz_url,'teamredminer-latest.tgz')
        
#class Claymore(Miner):
#    def download(self):
#        tgz_url = ''
#        #m = Mega.from_ephemeral()
#        m = Mega()
#        #m.login(email=os.environ['EMAIL'], password=os.environ['PASS'])
#        #url = 'https://mega.nz/#F!O4YA2JgD?KsJ2ESCK'
#        m.login()
#        url='https://mega.nz/#F!O4YA2JgD?KsJ2ESCK'
#        url='https://mega.nz/#F!O4YA2JgD!n2b4iSHQDruEsYUvTQP5_w?KsJ2ESCK'
#        claymore_folder_id = 'O4YA2JgD'
#        claymore_regex = r"Claymore\'s Dual Ethereum AMD\+NVIDIA GPU Miner v(\d+)\.(\d+) - LINUX\.tar\.gz"
#        is_claymore_tgz = re.compile(claymore_regex).match
#        files = m.get_files(public_folder_id=claymore_folder_id)
#        print(str(m.get_files('O4YA2JgD')))
#        print(str(files))
#        claymore_tgzs = [_file.a.n for _file in files if (_file.a.n and is_clamore_tgz(_file.a.n))]
#        print(claymore_tgzs)
#        
#        print("%s\n%s" % "-"*40, max(claymore_tgzs, key=lower))
#        """
{'h': 'iFlXFL6K', 'p': 'aE8FxRiQ', 'u': 'c1Vue7vR0eo', 't': 0, 'a': {'c': 's1pc3Tz4tRp49Sbac24HRwQNRpVa', 'n': "Claymore's Dual Ethereum+Decred_Siacoin_Lbry_Pascal_Blake2s_Keccak AMD+NVIDIA GPU Miner v11.1 - Catalyst 15.12-18.x - CUDA 8.0_9.1_7.5_6.5.zip"}, 'k': (3805463899, 26631005, 2882860955, 2454290455), 's': 27008847, 'ts': 1583691671, 'iv': (3784844781, 4154382804, 0, 0), 'meta_mac': (1006960888, 3227231920), 'key': (55238838, 4127752841, 2547119971, 1376921255, 3784844781, 4154382804, 1006960888, 3227231920)}
#        """
#        m.download_file(file_id='O4YA2JgD?KsJ2ESCK',file_key='n2b4iSHQDruEsYUvTQP5_w',public=True)
#        m.download_from_url(url)

def download(url,dest):
    a = urlparse(url)
    file_name = os.path.basename(a.path)
    logging.info("Downloading {} to {}".format(url, dest))
    with urllib.request.urlopen(url) as response, open(file_name, 'wb') as out_file:
        shutil.copyfileobj(response, out_file)
    #If tgz, unpack
    if os.path.splitext(file_name)[1]=='.tgz':
        with tarfile.open(file_name) as tar:
            tar.extractall()

def get_github_asset(repo: str, filename: str):
    logging.info("Getting latest from github")
    logging.info(args)
    # Read the first 64 bytes of the file inside the .gz archive located at `url`
    url = f'https://api.github.com/repos/{repo}'
    with urllib.request.urlopen(url) as response:
        data = json.loads(response.read().decode())
        logging.debug(data)
        release_tag = data['tag_name']
        asset = next (asset for asset in data['assets'] if asset['name'] == filename % (release_tag))
        logging.info ("Found {} as latest release".format ( release_tag))
        #Download
        tgz_url = asset['browser_download_url']
        return tgz_url
    

def main(args):
    """ Main entry point of the app """
    logging.basicConfig(level=10)
    if (args.miner == 'claymore'):
        miner = Claymore()
    elif args.miner == 'teamredminer':
        miner = Teamredminer()
    else:
        raise NotImplementedError
    miner.download()
    
    # with urllib.request.urlopen(url) as response:
    #     with gzip.GzipFile(fileobj=response) as uncompressed:
    #         file_header = uncompressed.read(64) # a `bytes` object
    #         # Or do anything shown above using `uncompressed` instead of `response`.



if __name__ == "__main__":
    """ This is executed when run from the command line """
    parser = argparse.ArgumentParser()

    # Required positional argument
    #parser.add_argument("arg", help="Required positional argument")

    # Optional argument flag which defaults to False
    # parser.add_argument("-f", "--flag", action="store_true", default=False)
    parser.add_argument("-m", "--miner", action="store", default="teamredminer", choices=('teamredminer','claymore'))
    # Optional argument which requires a parameter (eg. -d test)
    #parser.add_argument("-n", "--name", action="store", dest="name")

    # Optional verbosity counter (eg. -v, -vv, -vvv, etc.)
    parser.add_argument(
        "-v",
        "--verbose",
        action="count",
        default=0,
        help="Verbosity (-v, -vv, etc)")

    # Specify output of "--version"
    parser.add_argument(
        "--version",
        action="version",
        version="%(prog)s (version {version})".format(version=__version__))

    args = parser.parse_args()
    main(args)
